import './App.css';
import PDFReader from './components/PDFReader';
import Pdf from "./components/pdf.tsx"

function App() {
  return (
    <div className="App">
      <Pdf />
    </div>
  );
}

export default App;
